FROM golang:1.8-alpine

# copy go files to the the container, at gopath
COPY /pecuniaapi/* /go/src/pecuniaapi


# change to gopath
WORKDIR /go/src/pecuniaapi/

RUN mkdir -p /var/log/pecuniaapi

RUN mkdir /go/pkg

# adding git
RUN apk add --no-cache git mercurial bash

RUN go get github.com/golang/dep/cmd/dep

#Download
RUN dep ensure

RUN go install

RUN go build main.go

# run app
ENTRYPOINT ["/go/bin/pecuniaapi"]

# publiching app to 8080 port
EXPOSE 8080