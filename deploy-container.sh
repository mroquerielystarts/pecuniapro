#!/usr/bin/env bash
docker container stop  nmrk ## stop api container if is running
docker container rm nmrk ## delete api container if exist
docker build -t mairelin/pecunia-pro . ## build the container
## run the container
sudo docker run -d -p 8080:8080 --name nmrk --link nmrk-db-container-last  --net pecuniapro_nmrk-network  mairelin/pecunia-pro:latest