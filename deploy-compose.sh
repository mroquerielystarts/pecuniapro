#!/usr/bin/env bash
docker container stop nmrk  nmrk-db-container-last ## stop postgres and golang container is are running
docker container rm nmrk  nmrk-db-container-last ## delete postgres and golang container is exist
docker-compose up -d ## deploy componse