package model

type Note struct {
	ID int64   `model:"id" type:"bigserial" constraint:"note_pk PRIMARY KEY (id)" `
	Description string `model:"description" type:"text" mandatory:"true"`
	Title string `model:"title" type:"varchar(300)" mandatory:"true"`
	KeyWord string `model:"keyword" type:"varchar(100)" `
}


