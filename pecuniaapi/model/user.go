package model


type AuthUser struct {
	ID int64   `model:"id" type:"bigserial" constraint:"user_pk PRIMARY KEY (id)" `
	Nick string `model:"name" type:"varchar(100)" mandatory:"true"`
	UserName string `model:"username" type:"int" mandatory:"true"`
	Password string `model:"password" type:"int" mandatory:"true"`
}
