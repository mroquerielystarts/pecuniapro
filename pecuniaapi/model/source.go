package model

type Source struct {
	ID int64   `model:"id" type:"bigserial" constraint:"source_pk PRIMARY KEY(id)" `
	NoteId int64 `model:"note_id" type:"bigserial" mandatory:"true" reference:"note(ID)"`
	Type int `model:"type" type:"smallint" mandatory:"true"`
	Reference string `model:"reference" type:"text"`
}
