package migration

import (
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/model"
)

func RunMigrations(db *util.PostgresDB) []error {
	var resErrors []error
	resErrors = append(resErrors, db.CreateTable(&model.AuthUser{}))
	resErrors = append(resErrors, db.CreateTable(&model.Note{}))
	resErrors = append(resErrors, db.CreateTable(&model.Source{}))
	resErrors = append(resErrors, db.CreateSequence(model.Note{}))
	resErrors = append(resErrors, db.CreateSequence(model.AuthUser{}))
	resErrors = append(resErrors, db.CreateSequence(model.Source{}))
	return resErrors
}

func NeedMigrations(db *util.PostgresDB) (bool, error) {
	res := false
	var resErr error
	modelList :=  GetModelListOrdered()
	for _, model := range modelList  {
		exist, err := db.CheckIfExists(&model)

		if err == nil && !res {
			res = exist
			break
		}

		if err != nil {
			return res, err
		}
	}
	return res, resErr
}

func GetModelListOrdered() []interface{}{
	var modelList []interface{}
	modelList = append(modelList, model.Source{})
	modelList = append(modelList, model.Note{})
	modelList = append(modelList, model.AuthUser{})
	return modelList
}