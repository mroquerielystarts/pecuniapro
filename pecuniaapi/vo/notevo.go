package vo

type NoteVO struct {
	Description string `json:"description"`
	Title string `json:"title"`
	KeyWord string `json:"keyword"`
	UserId int64 `json:"user_id, omitempty"`
	Code int64 `json:"code, omitempty"`
}


