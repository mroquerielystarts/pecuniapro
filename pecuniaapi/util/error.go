package util

func CheckError(err error, resErrors *[]error) {
	if err != nil {
		*resErrors = append(*resErrors, err)
	}
}
