package util

import (
	"log"
	"io"
	"os"
	"time"
	"net/http"
		"errors"
	"fmt"
			)


type FreshLogger struct {
	LogDirectory string
}

var (
	trace   *log.Logger
	info    *log.Logger
	warning *log.Logger
	err   *log.Logger
	genInfo   *log.Logger
)


// Description: Initialize the the logguer
func (fl *FreshLogger) Init() error {
	var fileOut io.Writer
	var resError error
	if fl.LogDirectory != "" {
		fileOut = GetLoggerFile(fl.LogDirectory)
		info =    log.New(fileOut, "INFO: ", log.Ldate|log.Ltime)
		warning = log.New(fileOut, "WARNING: ", log.Ldate|log.Ltime)
		err =     log.New(fileOut, "ERROR: ", log.Ldate|log.Ltime)
	} else {
		fmt.Println("the log file name can't be null. Please set the value for FreshLogger.LogDirectory")
		resError = errors.New("the log file name can't be null. Please set the value for FreshLogger.LogDirectory")
	}
	return resError
}

func (logger FreshLogger) InfoR(requestId string, data interface{}, ) {
	info.Printf("RequestId: %s, Function: %s, Body: %s, ", requestId, GetFuncName(), data)
}

func (logger FreshLogger) WarningR(requestId string, data interface{}) {
	warning.Printf("RequestId: %s, Function: %s, Body: %s, ", requestId, GetFuncName(), data)
}

func (logger FreshLogger) ErrorR(requestId string, data interface{}) {
	err.Printf("RequestId: %s, Function: %s, Body: %s, ", requestId, GetFuncName(), data)
}

func (logger FreshLogger) GenPrintf(query string, data ...interface{}) {
	if data == nil {
		info.Printf(query)
	} else {
		info.Printf(query, data...)
	}
}

func (logger FreshLogger) Error(data string ) {
	err.Printf("GeneralInfo: %s ", data)
}

//Open or create the file for log, create a new file by day
func GetLoggerFile(fileName string) io.Writer {
	 var outFile io.Writer

	 currentName := fileName + "/fresh_" + time.Now().Format("2006-01-02") +".log"

		// detect if file exists
		var _, err = os.Stat(currentName)

		// create file if not exists
		if os.IsNotExist(err) {
			outFile , err = os.Create(currentName)
		} else {
			outFile, err = os.OpenFile(currentName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				log.Fatalf("error opening file: %v", err)
			}
		}
	return outFile
}


// Print request content
func (logger FreshLogger) PrintRequest(requestId string, r *http.Request) {
	// Add the request string else {}
	if r.Body == nil {
		info.Printf("RequestId: %v, Method: %v, URL: %v, Proto: %v, Host: %v, Headers: %v ",
			requestId, r.Method, r.URL, r.Proto, r.Host, r.Header)
	} else {
		info.Printf("RequestId: %v, Method: %v, URL: %v, Proto: %v, Host: %v, Headers: %v Body: %v",
			requestId, r.Method, r.URL, r.Proto, r.Host, r.Header, r.Form.Encode())
	}
}

// Print request content
func (logger FreshLogger) PrintResponse(requestId string, r interface{}, initTime time.Time) {
	info.Printf("RequestId: %v,  ResponseBody: %v, RequestExecutionTime: %v", requestId,  r, time.Since(initTime))
}
