package util

import (
	"time"
	"context"
)

// Returns a string type value of a given context key
// Params: context.Context: contains the map of value, Key Id of value
// Returns: the value string
func GetStringFromContext(requestContext context.Context, key string) string {
	val := requestContext.Value(key)
	var res string
	if  val != nil {
		res = val.(string)
	}
	return res
}

// Returns an time type value of a given context key
// Params: context.Context: contains the map of value, Key Id of value
// Returns: the value time
func GetTimeFromContext(requestContext context.Context, key string) time.Time {
	val := requestContext.Value(key)
	var res time.Time
	if  val != nil {
		res = val.(time.Time)
	}
	return res
}