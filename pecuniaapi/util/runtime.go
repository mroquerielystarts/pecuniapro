package util

import (
	"runtime"
)


// Description used for get the current
// method name that is running
func GetFuncName() string {
	var res string
	function, _, _, _:= runtime.Caller(2)
	if rf := runtime.FuncForPC(function); rf != nil {
		res =  rf.Name()
	}
	return res
}