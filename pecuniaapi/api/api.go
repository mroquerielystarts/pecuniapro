package api

import (
	"net/http"

	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"

		"github.com/gorilla/mux"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/controller"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/middleware"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/migration"
)

type ApiApp struct {
	Router *mux.Router
	DB *util.PostgresDB
	Logger *util.FreshLogger
}

func (app *ApiApp) InitRouters(controller controller.Controller, middleware middleware.Middleware)  *ApiApp {

	app.Router.HandleFunc("/notes", middleware.SimpleMiddleWare(controller.ListNotes)).Methods("GET")
	app.Router.HandleFunc("/notes", middleware.SecuredMiddleware(controller.CreateNote)).Methods("POST")
	app.Router.HandleFunc("/notes/{id:[0-9]+}", middleware.SimpleMiddleWare(controller.GetNoteById)).Methods("GET")
	app.Router.HandleFunc("/notes/{id:[0-9]+}", middleware.SecuredMiddleware(controller.UpdateNote)).Methods("PUT", "PATCH")
	app.Router.HandleFunc("/notes/{id:[0-9]+}", middleware.SecuredMiddleware(controller.DeleteNote)).Methods("DELETE")
	return app
}

func (app *ApiApp) Run(address string) {
	app.Logger.Init()
	db, errorConnecting := app.DB.InitDB()

	if errorConnecting == nil {
		app.Logger.GenPrintf("----Starting App-----  Database Data --> %s and Application Port --> %s ", app.DB.ConnString, address)

		app.CheckMigrations()

		controller := controller.Controller{DataBaseConn: &util.PostgresDB{DB: db}, Logger: app.Logger}
		middleWares := middleware.Middleware{Logger: app.Logger}

		app.InitRouters(controller, middleWares)

		err := http.ListenAndServe(address, app.Router)
		if err != nil {
			app.Logger.Error("Error trying to run the server: " + err.Error())
			return
		}
	} else {
		app.Logger.Error("Error trying to connect to database." + errorConnecting.Error())
	}
}

func (app *ApiApp) CheckMigrations() {
	res, _ := migration.NeedMigrations(app.DB)
	if res {
		app.Logger.GenPrintf("Will run migrations migrations")
		errs := migration.RunMigrations(app.DB)
		if errs != nil && len(errs) >0 {
			app.Logger.GenPrintf("Migrations executed")
		} else {
			app.Logger.ErrorR("Error running migrations.", errs)
		}

	} else {
		app.Logger.GenPrintf("Dont Need Migrations")
	}

}
