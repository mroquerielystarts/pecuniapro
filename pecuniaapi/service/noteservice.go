package service

import (
	"errors"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/model"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/vo"
	)

type NoteService struct {
	DB *util.PostgresDB
	Logger *util.FreshLogger
	RequestId string
}

func (bl NoteService) CreateNote(noteVO vo.NoteVO) (vo.NoteVO, []error) {
	var resErrors []error
	newNote := parseToModel(noteVO)
	err := bl.DB.Create(&newNote)
	util.CheckError(err, &resErrors)

	if len(resErrors) == 0  {

		util.CheckError(err, &resErrors)

	}

	return  noteVO, resErrors
}

func (bl NoteService) ListNotes(filters map[string]interface{}, offset int, limit int) ([]vo.NoteVO, []error) {
	var resErrors []error
	var list []vo.NoteVO

	if ok := offset > limit; ok {
		offset =  limit
	}

	//Get data from  bd
	rows, err := bl.DB.ListAllPaginated(&model.Note{}, limit, offset, filters)
	util.CheckError(err, &resErrors)

	if err == nil {
		defer rows.Close()

		for rows.Next() {
			var note model.Note
			//If not errors load data to object
			 err := rows.Scan(&note.ID, &note.Description, &note.Title, &note.KeyWord)
			 util.CheckError(err, &resErrors)

			 if err == nil {
				 list = append(list, parseToVO(note))
			 }

		}
	}

	return list, resErrors
}

func (bl NoteService) GetNoteById(id uint) (vo.NoteVO, error) {
	var resError error
	var res vo.NoteVO


	row :=  bl.DB.GetById(&model.Note{}, id)
	var note model.Note

	//If not errors load data to object
	if err := row.Scan(&note.ID, &note.Description, &note.Title, &note.KeyWord); err == nil {
		res = parseToVO(note)
	} else {
		bl.Logger.ErrorR(bl.RequestId, err.Error())
	}

	return res, resError
}

func (bl NoteService) UpdateNote(noteVO vo.NoteVO, id uint) []error {
	var resErrors []error

		model := parseToModel(noteVO)
		existentNote, _ := bl.GetNoteById(id)

		if (existentNote != vo.NoteVO{}) {
			_, err := bl.DB.Update(&model, id)
			util.CheckError(err, &resErrors)
		} else {
			util.CheckError(errors.New("that note does not exists"), &resErrors)
		}

	return resErrors
}

func (bl NoteService) DeleteNote(id uint) error {
	_, err := bl.DB.Delete(&model.Note{}, id)
	if err != nil {
		bl.Logger.ErrorR(bl.RequestId, err.Error())
	}
	return  err
}

func parseToModel(noteVO vo.NoteVO) model.Note {
	var res model.Note
	res.Description = noteVO.Description
	res.KeyWord = noteVO.KeyWord
	//res.UserId = noteVO.UserId
	return res
}

func parseToVO(note model.Note) vo.NoteVO {
	var res vo.NoteVO
	//res.UserId = note.UserId
	res.KeyWord = note.KeyWord
	res.Description = note.Description
	res.Title = note.Title
	res.Code = note.ID
	return res
}