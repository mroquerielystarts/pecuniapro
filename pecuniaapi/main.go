package main

import (
	"fmt"
		"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/api"
	"github.com/gorilla/mux"
)

func main() {

	connString := fmt.Sprintf("host=%s port=%s user=%s  password=%s dbname=%s sslmode=disable",
		"172.19.0.2", "5432", "pecuniatest", "pecuniatest", "pecuniatest")

	dataBase := util.PostgresDB {ConnString: connString, Driver: "postgres"}

	a := api.ApiApp{Router: mux.NewRouter(), DB: &dataBase, Logger: &util.FreshLogger{ LogDirectory: "/var/log/pecuniaapi"}}

	a.Run(":8080")

	if dataBase.DB != nil {
		defer dataBase.Close()
	}
}