package controller

import (
	"net/http"
	"encoding/json"
	"strconv"
	"context"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/vo"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/service"
	"github.com/gorilla/mux"
)

type Controller struct {
	DataBaseConn *util.PostgresDB
	Logger *util.FreshLogger
}

func (controller Controller) CreateNote(rw http.ResponseWriter, req *http.Request, ctx context.Context)  {
	var resErrors []error
	var resData vo.NoteVO

	decoder := json.NewDecoder(req.Body)

	noteVO := vo.NoteVO{}
	util.CheckError(decoder.Decode(&noteVO), &resErrors)


	if len(resErrors) == 0 {
		noteSrv := service.NoteService{ DB: controller.DataBaseConn, Logger: controller.Logger, RequestId: util.GetStringFromContext(ctx, "request-id")}
		resData, resErrors = noteSrv.CreateNote(noteVO)
	}

	CloseRequestBody(req)
	controller.constructResponse(req, ctx, resErrors, rw, "Error creating note: ",  resData)
}

func (controller Controller) ListNotes(rw http.ResponseWriter, req *http.Request, ctx context.Context) {
	filters := make(map[string]interface{})

	title := req.URL.Query().Get("title")

	if len(title) > 0 {
		filters["title"] = title
	}

	keyword := req.URL.Query().Get("keyword")
	if len(keyword) > 0 {
		filters["keyword"] = keyword
	}


	limit, _ := strconv.Atoi(req.URL.Query().Get("limit"))
	offset, _ := strconv.Atoi(req.URL.Query().Get("offset"))

	noteSrv := service.NoteService{ DB: controller.DataBaseConn, Logger: controller.Logger, RequestId: util.GetStringFromContext(ctx, "request-id")}
	res, resErrors := noteSrv.ListNotes(filters, offset, limit)


	CloseRequestBody(req)
	controller.constructResponse(req, ctx, resErrors, rw, "Error listing notes ",  res)

}

func (controller Controller) GetNoteById(rw http.ResponseWriter, req *http.Request, ctx context.Context) {
	var errors  []error
    var res vo.NoteVO
	//Getting request body
	vars := mux.Vars(req)
	id, err := strconv.ParseUint(vars["id"], 10, 64)

	if err == nil {
		//go business layer
		noteService := service.NoteService{DB: controller.DataBaseConn, Logger: controller.Logger, RequestId: util.GetStringFromContext(ctx, "request-id")}
		res, err = noteService.GetNoteById(uint(id))
		util.CheckError(err, &errors)
	}

	controller.constructResponse(req, ctx, errors, rw, "Error getting note.", res)
}

func (controller Controller) DeleteNote(rw http.ResponseWriter, req *http.Request, ctx context.Context) {
	var resErrors []error

	//Getting request url
	vars := mux.Vars(req)
	id, err := strconv.ParseUint(vars["id"], 10, 64)

	if err == nil {
		//go business layer
		noteService := service.NoteService{DB: controller.DataBaseConn, Logger: controller.Logger, RequestId: util.GetStringFromContext(ctx, "request-id")}
		err := noteService.DeleteNote(uint(id))
		util.CheckError(err, &resErrors)
	}

	controller.constructResponse(req, ctx, resErrors, rw, "Error deleting Note: ", nil)
}

func (controller Controller) UpdateNote(rw http.ResponseWriter, req *http.Request, ctx context.Context) {
	var resErrors []error

	//Getting request url
	vars := mux.Vars(req)
	id, err := strconv.ParseUint(vars["id"], 10, 64)

	if err == nil {
		//go business layer
		noteService := service.NoteService{DB: controller.DataBaseConn, Logger: controller.Logger, RequestId: util.GetStringFromContext(ctx, "request-id")}
		err := noteService.DeleteNote(uint(id))
		util.CheckError(err, &resErrors)
	}

	controller.constructResponse(req, ctx, resErrors, rw, "Error deleting note: ", nil)
}


func CloseRequestBody(req *http.Request) {
	if req.ContentLength > 0 {
		req.Body.Close()
	}
}

func (controller Controller) constructResponse(req *http.Request, ctx context.Context, errors []error, rw http.ResponseWriter,
	errorPrefix string, resData interface{}) {
	js, err := json.Marshal(resData)
	util.CheckError(err, &errors)

	rw.Header().Set("Content-Type", "application/json")

	if len(errors) > 0 {
		errMsg := errorPrefix
		for _, err := range errors {
			errMsg = errMsg + " " + err.Error() + ", "
		}
		http.Error(rw, errMsg, http.StatusBadRequest)
	} else {
		rw.Write(js)
	}

}
