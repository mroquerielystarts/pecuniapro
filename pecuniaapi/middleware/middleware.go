package middleware

import (
	"net/http"
	"context"
	"time"
		"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"
	"github.com/satori/go.uuid"
)

type Middleware struct {
	Logger *util.FreshLogger
}

func (mw Middleware) SecuredMiddleware(next func(w http.ResponseWriter, r *http.Request, cxt context.Context)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestContext := constructContext()

		reqId := util.GetStringFromContext(requestContext, "request-id")
		mw.Logger.PrintRequest(reqId, r)

		if mw.Auth(w, r, requestContext) {
			next(w, r, requestContext)
		}
	}
}

func (mw Middleware) SimpleMiddleWare(next func(w http.ResponseWriter, r *http.Request, cxt context.Context)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestContext :=  constructContext()
		next(w, r, requestContext)
	}
}

func (mw Middleware) Auth(w http.ResponseWriter, r *http.Request, ctx context.Context) bool {
	user, pass, _ := r.BasicAuth()

	if user !=  "pecuniatest"|| pass !=  "pecuniatest" {

		w.Header().Set("WWW-Authenticate", `Basic realm = "Provide user and pass"`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Unauthorised.\n"))

		mw.Logger.WarningR(ctx.Value("request-id").(string), user + " Unauthorized!")

		return false
	}

	mw.Logger.InfoR(ctx.Value("request-id").(string), user + " Authorized!")
	return true
}

func constructContext() context.Context {
	var res context.Context
	newRequestId := uuid.NewV4()
	res = context.WithValue(context.Background(), "request-id", newRequestId.String())
	return context.WithValue(res, "init-time", time.Now())
}