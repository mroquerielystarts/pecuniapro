package test

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/gorilla/mux"
		"bytes"
	"fmt"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/api"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/util"
	controller2 "bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/controller"
	"bitbucket.org/mroquerielystarts/pecuniapro/pecuniaapi/middleware"
	)

func InitApp() api.ApiApp {
	connString := fmt.Sprintf("host=%s port=%s user=%s  password=%s dbname=%s sslmode=disable", "localhost", "5432", "pecuniatest", "pecuniatest", "pecuniatest")
	db := util.PostgresDB{ConnString:connString, Driver: "postgres"}
	app :=  api.ApiApp{Router: mux.NewRouter(), DB: &db, Logger: &util.FreshLogger{ LogDirectory: "test"}}
	app.DB.InitDB()
	app.Logger.Init()
	app.CheckMigrations()
	controller :=  controller2.Controller{DataBaseConn: &db, Logger: app.Logger}
	middleWares :=  middleware.Middleware{Logger: app.Logger}
	app.InitRouters(controller, middleWares)
	return app
}

func TestApiAll(t *testing.T) {
	t.Run("List", TestListNoteController)
	t.Run("Get", TestGetByIdNoteController)
	t.Run("Create", TestCreateNoteController)
	t.Run("Update", TestUpdateNoteController)
	t.Run("rate", TestRatingController)
}


func TestRuntApp(t *testing.T)  {
	connString := fmt.Sprintf("host=%s port=%s user=%s  password=%s dbname=%s sslmode=disable", "localhost", "hellofresh", "hellofresh", "hellofresh", "5432")
	db := util.PostgresDB{ConnString:connString, Driver: "postgres"}
	a :=  api.ApiApp{Router: mux.NewRouter(), DB: &db, Logger: &util.FreshLogger{ LogDirectory: "fresh"}}
	a.Run(":8080")

}

func TestListNoteController(t *testing.T)  {
	request, _ := http.NewRequest("GET", "/notes", nil)
	response := executeRequest(request)
	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func TestListFilterNoteController(t *testing.T)  {
	request, _ := http.NewRequest("GET", "/notes", nil)
	response := executeRequest(request)
	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func TestCreateNoteController(t *testing.T)  {
	json :=  []byte(`{"title": "Dia del pobre", "description": "aprendi que el dia del pobre es hoy"}`)
	request, _ := http.NewRequest("POST", "/notes", bytes.NewBuffer(json))
	response := executeRequest(request)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func TestGetByIdNoteController(t *testing.T)  {
	request, _ := http.NewRequest("GET", "/notes/13", nil)
	response := executeRequest(request)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func TestUpdateNoteController(t *testing.T)  {
	json :=  []byte(`{"title": "Dia del rico", "description": "aprendi que el dia del rico es hoy"}`)

	request, _ := http.NewRequest("PATCH", "/notes/5", bytes.NewBuffer(json))
	response := executeRequest(request)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func TestDeleteController(t *testing.T)  {
	request, _ := http.NewRequest("DELETE", "/notes/12", nil)
	response := executeRequest(request)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func TestRatingController(t *testing.T)  {
	request, _ := http.NewRequest("POST", "/notes/13/rating/1", nil)
	response := executeRequest(request)
	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	} else {
		t.Log(response.Body.String())
	}
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	req.SetBasicAuth("pecuniatest", "pecuniatest")
	InitApp().Router.ServeHTTP(rr, req)
	return rr
}